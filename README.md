SymBA is built on top of (an old version of) the Fast Downward Planning System (http://www.fast-downward.org/), so it is made available under the GNU Public License (GPL).

This version (SymBA-2) is the winner of IPC'2014: https://helios.hud.ac.uk/scommv/IPC-14/

# Usage

Use ./build to compile and ./plan <domain.pddl> <problem.pddl> <plan_file> to execute

By default these scripts run the SymBA-2 version of the planner. To Run SymBA-1 simply modify the plan script replacing seq-opt-symba-2 by seq-opt-symba-1
# Branches

There are three branches that only differ in minor details of the configuration of the build and plan scripts:
* Symba-2-32bits: Original version of SymBA-2 compiled in 32 bits (requires installing the dependencies of gcc for 32 bits)
* Symba-2-64bits: Version modified to compile in 64 bits (may be slightly easier to compile and/or configure, though perhaps it is also slightly less efficient in terms of memory and/or runtime depending on the hardware).

# References
* Álvaro Torralba, Vidal Alcázar, Daniel Borrajo, Peter Kissmann and Stefan Edelkamp. 2014. SymBA*: A Symbolic Bidirectional A* planner. In International Planning Competition, 105–108.
* Álvaro Torralba, Carlos Linares López, and Daniel Borrajo: Abstraction Heuristics for Symbolic Bidirectional Search. IJCAI 2016. 3272-3278



If you find a compilation error or bug feel free to contact me. The codebase is quite old so for new developments I recommend to use the more modern variant (https://gitlab.com/atorralba/fast-downward-symbolic). However, the more modern variant does not contain the SymBA algorithm that was used in IPC'14 so if you want to reproduce those results, use this version!


Note: If you're using the FD-lab (http://lab.readthedocs.io/en/latest/) scripts to run experiments, there are a couple of things that have to be changed (in case that you run the planner calling the plan script that we provide you can ignore all this). We made changes on the translator and preprocessor so you cannot re-use the preprocessed files from another planners. Also, the preprocessor needs to receive "--opt-ordering" as parameter. Our plan script already does that, but using FD-lab will skip that part so a workaround is needed (e.g, modify the code of preprocess to put opt-ordering to true as default).


We incorporate the version 2.5 of the CUDD library, which was the one used at IPC'14. There are newer versions available of the library.
